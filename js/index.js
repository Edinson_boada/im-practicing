$(function() 
    	{
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel({
  				interval: 2000
  			});

        $('#contacto').on('show.bs.modal',function(e)
        {
          console.log("El modal se esta mostrando");
          $('#contactoBtn').removeClass('btn-outline-success');
          $('#contactoBtn').addClass('btn-secondary');          
          $('#contactoBtn').prop('disabled',true);   
          //$('#contactoBtn').('btn-outline-success');
          //$('#contactoBtn').removeClass('btn-outline-success');
        });

        $('#contacto').on('shown.bs.modal',function(e)
        {
          console.log("El modal se ha mostrado");
        });

        $('#contacto').on('hide.bs.modal',function(e)
        {
          console.log("El modal se esta ocultando");
        });

        $('#contacto').on('hidden.bs.modal',function(e)
        {
          console.log("El modal se ha ocultado");
          $('#contactoBtn').prop('disabled',false);  
          $('#contactoBtn').removeClass('btn-secondary');
          $('#contactoBtn').addClass('btn-outline-success');
        });
		});